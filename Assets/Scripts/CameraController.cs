using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System;

public class CameraController : MonoBehaviour
{
    public bool heat = true;
    public bool water = true;
    public bool cool = true;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
            Application.Quit();
        if (Input.GetKey(KeyCode.A))
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y - 100 * Time.deltaTime,
                0));
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y+100*Time.deltaTime,
                0));
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y,
                transform.position.z + 10 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y,
                transform.position.z - 10 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 10 * Time.deltaTime,
                transform.position.z);
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 10 * Time.deltaTime,
                transform.position.z);
        }
    }
}
