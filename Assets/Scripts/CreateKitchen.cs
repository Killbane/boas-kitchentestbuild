using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Newtonsoft.Json;

public class CreateKitchen : MonoBehaviour
{
    public float ModuleMinimalSize = 0;
    public float ModuleMaximalSize = 0;
    public float Length = 0;
    public float LeftLength = 0;
    public float RightLength = 0;
    public float Depth = (float)0.5;
    public float Height;
    public string DataFile;

    public GameObject simpleObjectPrefab;
    public GameObject TextPrefab;
    private List<GameObject> Objects;
    public Material Mat;
    private AutoBuilder builder;
    private const string filePath = "TestsDir/";
    // Start is called before the first frame update
    void Start()
    {
        builder = new AutoBuilder()
        {
            Length = this.Length,
            LeftLength = this.LeftLength,
            RightLength = this.RightLength,
            Heght = this.Height,
            Depth = this.Depth,
            MaximalModuleSize = this.ModuleMaximalSize,
            MinimalModuleSize = this.ModuleMinimalSize
        };
        Objects = new List<GameObject>();
        Rebuild();
    }

    public void Rebuild()
    {
        var script = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
        script.cool = true;
        script.heat = true;
        script.water = true;

        while (Objects.Count > 0)
        {
            Destroy(Objects[0]);
            Objects.RemoveAt(0);
        }

        List<Module> list;

        if (File.Exists(filePath + DataFile))
        {
            var reader = new StreamReader(filePath + DataFile);
            var first = reader.ReadLine().Split();
            Length = float.Parse(first[0]);
            LeftLength = float.Parse(first[1]);
            RightLength = float.Parse(first[2]);
            var str = reader.ReadToEnd();
            reader.Close();
            list = JsonConvert.DeserializeObject<List<Module>>(str);
            //CheckForErrors(list);
            foreach (var each in list)
            {
                each.Name = each.Name.Replace(' ', '\n');
            }
        }
        else
            return;
        var log = new StreamWriter("log.txt");
        log.WriteLine($"{Length} {LeftLength} {RightLength}");
        log.Write(JsonConvert.SerializeObject(list));
        log.Close();
        //left -1
        GameObject obj;
        GameObject txt;
        //var lineDepth = list.First(x => x.Line == 0&&x.Coords.y==0).Size.z;
        var lineDepth = new float[] { 0, 0, 0 };
        lineDepth[0] = list.First(x => x.Line == 0 && x.Coords.y == 0).Size.z;
        if (list.Any(x => x.Line == 1 && x.Coords.y == 0))
            lineDepth[1] = list.First(x => x.Line == 1 && x.Coords.y == 0).Size.z;
        if (list.Any(x => x.Line == -1 && x.Coords.y == 0))
            lineDepth[2] = list.First(x => x.Line == -1 && x.Coords.y == 0).Size.z;

        foreach (var module in list)
        {
            var ater = new Material(Mat);
            ater.SetColor("_Color", (Color32)module.material);
            switch (module.Line)
            {
                case -1:
                    obj = Instantiate(simpleObjectPrefab, new Vector3(Length / 2 - module.Size.z / 2, module.Coords.y + module.Size.y / 2, LeftLength - module.Coords.x + lineDepth[0]),
                        Quaternion.Euler(0, -90, 0), this.transform);
                    break;
                case 1:
                    obj = Instantiate(simpleObjectPrefab, new Vector3(-Length / 2 + module.Size.z / 2, module.Coords.y + module.Size.y / 2, module.Coords.x + lineDepth[0]),
                        Quaternion.Euler(0, 90, 0), this.transform);
                    break;
                default:
                    obj = Instantiate(simpleObjectPrefab, new Vector3(Length - (module.Coords.x + Length / 2), module.Coords.y + module.Size.y / 2, module.Size.z / 2),
                        Quaternion.Euler(0, 0, 0), this.transform);
                    break;
            }

            obj.transform.localScale = module.Size;
            txt = Instantiate(TextPrefab, obj.transform);
            txt.GetComponent<TextMesh>().text = module.Name;
            obj.GetComponent<MeshRenderer>().material = ater;

            Objects.Add(obj);
        }
    }

    private void CheckForErrors(List<Module> list)
    {
        var errorMat = new MyColor(255, 0, 0, 255);
        var lst = list.OrderBy(c => c.Coords.y).ThenBy(c => c.Line).ThenBy(c => c.Coords.x).ToArray();
        for (int i = 0; i < lst.Length - 1; i++)
        {
            if (lst[i].Line == lst[i + 1].Line && lst[i].Coords.y == lst[i + 1].Coords.y)
            {
                if (Math.Abs(lst[i].Coords.x - lst[i + 1].Coords.x) < (lst[i].Size.x + lst[i + 1].Size.x) / 2 - 0.000001)
                {
                    lst[i].material = errorMat;
                    lst[i].Name = "Error";
                    lst[i + 1].material = errorMat;
                    lst[i + 1].Name = "Error";
                }
            }
        }
    }

    public void LengthChange(string newValue)
    {
        float data;
        if (float.TryParse(newValue, out data))
        {
            Length = data;
            builder.Length = data;
        }
    }

    public void LeftLengthChange(string newValue)
    {
        float data;
        if (float.TryParse(newValue, out data))
        {
            LeftLength = data;
            builder.LeftLength = data;
        }
    }

    public void RightLengthChange(string newValue)
    {
        float data;
        if (float.TryParse(newValue, out data))
        {
            RightLength = data;
            builder.RightLength = data;
        }
    }
    public void DataFileChange(string newValue)
    {
        DataFile = newValue;
    }

}
