using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class AutoBuilder : MonoBehaviour
{
    public List<Module> SpecialModules;
    public float Depth;
    public float Heght;
    public float Length;
    public float LeftLength;
    public float RightLength;
    public float MinimalModuleSize;
    public float MaximalModuleSize;

    public AutoBuilder()
    {
        SpecialModules = new List<Module>();
    }

    public List<Module> AutoBuild()
    {
        var list = new List<Module>();
        list.AddRange(BuildOnLine(Length,0));
        list.AddRange(BuildOnLine(LeftLength,-1));
        list.AddRange(BuildOnLine(RightLength, 1));
        return list;
    }

    private List<Module> ClearBuild(float length, int line)
    {
        var cnt = (float)Math.Ceiling(2 * length / (MinimalModuleSize + MaximalModuleSize));
        var moduleSize = length / cnt;
        var begin = moduleSize / 2;
        var list = new List<Module>();
        var newMaterial = new MyColor(10, 231, 42,255);
        for (int i = 0; i < cnt; i++)
        {
            list.Add(new Module()
            {
                Name = "",
                Size = new MyVector(moduleSize, Heght, Depth),
                Coords = new MyVector(begin,0,0),
                Line = line,
                material = newMaterial
            });

            begin += moduleSize;
        }

        return list;
    }

    private List<Module> BuildOnLine(float length, int line)
    {
        var list = new List<Module>();
        var specialObjects = SpecialModules.FindAll(c => c.Line == line).OrderBy(c=>c.Coords.x).ToList();
        float edge;
        float cnt;
        float begin;
        float moduleSize;
        var newMaterial =  new MyColor(10, 231, 42,255);

        if (specialObjects.Count == 0)
            return ClearBuild(length, line);

        if (specialObjects[0].Coords.x - specialObjects[0].Size.x / 2 < MinimalModuleSize)
        {
            specialObjects[0].Coords = new MyVector(specialObjects[0].Size.x, 0, 0);
            specialObjects[0].Line = line;
        }



        if (length - specialObjects[specialObjects.Count - 1].Coords.x - specialObjects[0].Size.x / 2 < MinimalModuleSize)
        {
            specialObjects[specialObjects.Count - 1].Coords.x = length - specialObjects[0].Size.x / 2;
            specialObjects[specialObjects.Count - 1].Line = line;
        }

        for (int i = 0; i < specialObjects.Count - 1; i++)
        {
            if (Math.Abs(specialObjects[i + 1].Coords.x - specialObjects[i].Coords.x) -
                specialObjects[i + 1].Size.x - specialObjects[i].Size.x < MinimalModuleSize)
            {
                var delta = (float)Math.Max(
                    (-(specialObjects[i + 1].Coords.x - specialObjects[i].Coords.x -
                       -specialObjects[i + 1].Size.x - specialObjects[i].Size.x) +
                     MinimalModuleSize) / 2, 0.02);

                specialObjects[i].Coords.x = specialObjects[i].Coords.x - delta;
                specialObjects[i+1].Coords.x = specialObjects[i + 1].Coords.x + delta;
                
                return BuildOnLine(length, line);
            }

            edge = specialObjects[i].Coords.x + specialObjects[i].Size.x / 2;
            cnt = (float)Math.Ceiling(2 * (specialObjects[i + 1].Coords.x - specialObjects[i + 1].Size.x/2 - edge) / 
                                      (MinimalModuleSize + MaximalModuleSize));
            moduleSize = (specialObjects[i + 1].Coords.x - specialObjects[i + 1].Size.x/2 - edge) / cnt;
            begin = edge + moduleSize / 2;
            for (int j = 0; j < cnt; j++)
            {
                list.Add(new Module()
                {
                    Name = "",
                    Size = new MyVector(moduleSize, specialObjects[i].Size.y, Depth),
                    Coords = new MyVector(begin, 0, 0),
                    Line = line,
                    material = newMaterial
                });
                begin += moduleSize;
            }
        }

        edge = specialObjects[0].Coords.x - specialObjects[0].Size.x / 2;
        cnt = (float)Math.Ceiling(2 * (edge) / (MinimalModuleSize + MaximalModuleSize));
        moduleSize = (edge) / cnt;
        if (moduleSize < MinimalModuleSize)
            cnt = 0;
        begin = moduleSize / 2;
        for (int i = 0; i < cnt; i++)
        {
            list.Add(new Module()
            {
                Name = "",
                Size = new MyVector(moduleSize, specialObjects[0].Size.y, Depth),
                Coords = new MyVector(begin, 0, 0),
                Line = line,
                material = newMaterial
            });
            begin += moduleSize;
        }

        edge = specialObjects[specialObjects.Count - 1].Coords.x +
                     specialObjects[specialObjects.Count - 1].Size.x / 2;
        cnt = (float)Math.Ceiling(2 * (length - edge) / (MinimalModuleSize + MaximalModuleSize));
        moduleSize = (length - edge) / cnt;
        if (moduleSize < MinimalModuleSize)
            cnt = 0;
        begin = edge + moduleSize / 2;
        for (int i = 0; i < cnt; i++)
        {
            list.Add(new Module()
            {
                Name = "",
                Size = new MyVector(moduleSize, specialObjects[0].Size.y, Depth),
                Coords = new MyVector(begin, 0, 0),
                Line = line,
                material = newMaterial
            });
            begin += moduleSize;
        }

        return list;
    }
}
