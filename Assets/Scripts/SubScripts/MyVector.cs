﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MyVector
{
    public float x;
    public float y;
    public float z;

    public MyVector() {}

    public MyVector(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static MyVector operator +(MyVector a, MyVector b)
    {
        return new MyVector(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static implicit operator Vector3(MyVector c)
    {
        return new Vector3(c.x, c.y, c.z);
    }
}
