﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MyColor
{
    public byte r;
    public byte g;
    public byte b;
    public byte? a;

    public MyColor(){}
    public MyColor(byte r, byte g, byte b, byte? a = null)
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public static implicit operator Color32(MyColor c)
    {
        if (c.a == null)
        {
            return new Color32(c.r, c.g, c.b, 255);
        }
        else
        {
            return new Color32(c.r, c.g, c.b, (byte) c.a);
        }
    }
}
