﻿using System;

[Serializable]
public class Module
{
    public string Name = "";
    public MyVector Size;
    public MyVector Coords;
    public int Line;
    public MyColor material;
    public bool IsUper;
}