﻿public class Pair<T1, T2>
{
    public T1 First;
    public T2 Second;
    public static implicit operator Pair<T1,T2>((T1,T2) c)
    {
        return new Pair<T1, T2>
        {
            First = c.Item1, 
            Second = c.Item2
        };
    }
}
